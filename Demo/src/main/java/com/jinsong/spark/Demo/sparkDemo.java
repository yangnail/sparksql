package com.jinsong.spark.Demo;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import static org.apache.spark.sql.functions.col;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class sparkDemo {


    public static void main(String[] args) {
//
//    	 SparkSession spark = SparkSession
//                 .builder()
//                 .master("local")
//                 .appName("Java Spark SQL basic example")
//                 .config("spark.sql.warehouse.dir", "D:\\work\\bigdata\\code\\scala\\Demo\\spark-warehouse")
//                 .getOrCreate();

    	 String input = "D:\\work\\bigdata\\code\\scala\\sparksql\\Demo\\people.json";
//    	 Person person = new Person();
//    	 person.setName("Andy");
//    	 person.setAge(32);
    	 
//    	 Encoder<Person> personEncoder = Encoders.bean(Person.class);
//    	 Dataset<Person> javaBeanDS = spark.createDataset(
//    	   Collections.singletonList(person),
//    	   personEncoder
//    	 );
//    	 javaBeanDS.show();
    	 
    	 
    	// Encoders for most common types are provided in class Encoders
//    	 Encoder<Integer> integerEncoder = Encoders.INT();
//    	 Dataset<Integer> primitiveDS = spark.createDataset(Arrays.asList(1, 2, 3), integerEncoder);
//    	 Dataset<Integer> transformedDS = primitiveDS.map(
//    	     (MapFunction<Integer, Integer>) value -> value + 1,
//    	     integerEncoder);
//    	 transformedDS.collect(); // Returns [2, 3, 4]
//    	 transformedDS.show();

    	 // DataFrames can be converted to a Dataset by providing a class. Mapping based on name
//    	 String path = "examples/src/main/resources/people.json";
//    	 Dataset<Person> peopleDS = spark.read().option("timestampFormat", "yyyy/MM/dd HH:mm:ss ZZ").json(input).as(personEncoder);
//    	 peopleDS.show();
////    	 
//        Dataset<Row> df = spark.read().option("timestampFormat", "yyyy/MM/dd HH:mm:ss ZZ").json(input);
//    	  Dataset<Row> df=null;
//        df.createOrReplaceTempView("people1");
//        Dataset<Row> sqlDF = spark.sql("SELECT * FROM people1");
//        sqlDF.show();

//        df.show();
//        df.printSchema();
//        df.select("name").show();
//
//        df.select(col("name"), col("age").plus(1)).show();
//        df.filter(col("age").gt(21)).show();
//        df.filter(col("age").gt(22)).show();
//        df.groupBy(col("age")).count().show();
    
    	 String warehouseLocation = new File("spark-warehouse").getAbsolutePath();
    	 SparkSession spark = SparkSession
    			  .builder()
    			  .appName("Java Spark Hive Example")
    			  .config("spark.sql.warehouse.dir", warehouseLocation)
    			  .enableHiveSupport()
    			  .getOrCreate();
    	 spark.sql("CREATE TABLE IF NOT EXISTS src (key INT, value STRING) USING hive");
    	 spark.sql("LOAD DATA LOCAL INPATH 'kv1.txt' INTO TABLE src");
    	 spark.sql("SELECT * FROM src").show();
    	 spark.sql("SELECT COUNT(*) FROM src").show();
    	 
    	// The results of SQL queries are themselves DataFrames and support all normal functions.
    	 Dataset<Row> sqlDF = spark.sql("SELECT key, value FROM src WHERE key < 10 ORDER BY key");

    	 // The items in DataFrames are of type Row, which lets you to access each column by ordinal.
    	 Dataset<String> stringsDS = sqlDF.map(
    	     (MapFunction<Row, String>) row -> "Key: " + row.get(0) + ", Value: " + row.get(1),
    	     Encoders.STRING());
    	 stringsDS.show();
    	 // +--------------------+
    	 // |               value|
    	 // +--------------------+
    	 // |Key: 0, Value: val_0|
    	 // |Key: 0, Value: val_0|
    	 // |Key: 0, Value: val_0|
    	 // ...

    	 // You can also use DataFrames to create temporary views within a SparkSession.
    	 List<Record> records = new ArrayList<>();
    	 for (int key = 1; key < 100; key++) {
    	   Record record = new Record();
    	   record.setKey(key);
    	   record.setValue("val_" + key);
    	   records.add(record);
    	 }
    	 Dataset<Row> recordsDF = spark.createDataFrame(records, Record.class);
    	 recordsDF.createOrReplaceTempView("records");

    	 // Queries can then join DataFrames data with data stored in Hive.
    	 spark.sql("SELECT * FROM records r JOIN src s ON r.key = s.key").show();
    }

}
