package com.jinsong.spark.Demo;

import org.apache.spark.sql.SparkSession;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        
        SparkSession spark = SparkSession
        		  .builder()
        		  .appName("Java Spark SQL basic example")
        		  .config("spark.some.config.option", "some-value")
        		  .getOrCreate();
    }
}
